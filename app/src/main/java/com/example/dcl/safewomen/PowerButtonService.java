package com.example.dcl.safewomen;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Created by User on 5/1/2018.
 */

public class PowerButtonService extends Service {

    public PowerButtonService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        LinearLayout mLinear = new LinearLayout(getApplicationContext()) {

            //home or recent button
            public void onCloseSystemDialogs() {

                /*

                String message = "My current location is:" + "\t" + "Hello There" ;
                //722684021
                String phoneNo = "+8801722";

                StringTokenizer st=new StringTokenizer(phoneNo,",");
                while (st.hasMoreElements())
                {
                    String tempMobileNumber = (String)st.nextElement();
                    if(tempMobileNumber.length()>0 && message.trim().length()>0) {
                        sendSMS( message ,tempMobileNumber);
                    }
                    else
                    {
                        Toast.makeText(getBaseContext(),
                                "Please enter both phone number and message.",
                                Toast.LENGTH_SHORT).show();
                    }
                }

                */

                sendSMS();


            }

            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {

                if(event.getKeyCode()== KeyEvent.KEYCODE_VOLUME_UP||event.getKeyCode()== KeyEvent.KEYCODE_VOLUME_DOWN){

                    Log.i("Key", "keycode " + event.getKeyCode());
                    onCloseSystemDialogs();
                }
                return super.dispatchKeyEvent(event);

                /*
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
                        || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP
                        || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN
                        || event.getKeyCode() == KeyEvent.KEYCODE_CAMERA
                        || event.getKeyCode() == KeyEvent.KEYCODE_POWER) {
                    Log.i("Key", "keycode " + event.getKeyCode());
                    onCloseSystemDialogs();
                }

                return super.dispatchKeyEvent(event);


                */
            }






        };

        mLinear.setFocusable(true);

        View mView = LayoutInflater.from(this).inflate(R.layout.service_layout, mLinear);
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        //params
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                100,
                100,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_FULLSCREEN
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
        wm.addView(mView, params);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendSMS(){

        SmsManager sms = SmsManager.getDefault();
        String msg, phn;


         msg = "My current location is:" + "\t" + "Hello There" ;
        //722684021
        phn = "+8801722684021";
        sms.sendTextMessage(phn,null,msg,null,null);
        Toast.makeText(this, "Message send successfully", Toast.LENGTH_SHORT).show();
    }

}




