package com.example.dcl.safewomen;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dcl.safewomen.capture.ImageCaptureService;
import com.example.dcl.safewomen.configurations.ConfigActivity;
import com.example.dcl.safewomen.configurations.ConfigPreferences;
import com.example.dcl.safewomen.model.MyDBHandler;
import com.example.dcl.safewomen.scheduler.AlarmReceiver;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.seismic.ShakeDetector;

import static com.example.dcl.safewomen.HomePage.selectedIntervalMultiplier;

public class ProfileActivity extends AppCompatActivity implements ShakeDetector.Listener{

    TextView txt;
    ImageButton b;
    FirebaseAuth firebaseAuth;
    ImageView location;
    Button next;
    public final static int REQUEST_CODE = 10101;
    boolean flag = false;

    boolean flag2 = false;
    private Button edit,save;

    EditText num1,num2,num3;
    private Context c;
    MediaPlayer mediaPlayer;
    MyDBHandler dbHandler;
    int i=0;
    Vibrator v;
    private TextView logout;
    Button video;
    Button search;
    private TextView guide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        next=(Button)findViewById(R.id.next);
        dbHandler = new MyDBHandler(this,null,null,1);
        logout=(TextView)findViewById(R.id.log);
        guide=(TextView)findViewById(R.id.guideline);

        //video=(Button)findViewById(R.id.bbk);



        //b=(ImageButton) findViewById(R.id.out);
        //next.setOnClickListener(this);


        nextActivity();
        logoutActivity();
        guideLineAMethod();
        //videoBack();
        

        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));

        }

        FirebaseUser user = firebaseAuth.getCurrentUser();
        txt=(TextView)findViewById(R.id.userId);
        txt.setText("Hi~ "+ user.getEmail());



    }



    private void guideLineAMethod() {

        guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mybuilder= new AlertDialog.Builder(ProfileActivity.this);
                mybuilder.setTitle("How to use this apps?");
                mybuilder.setMessage("1.Shake phone 3 times 180 degree angle.\nAutomatic open camera and capture image\nSend sms to trusted contact number\nCall to 999\n2.Share location to social media and and send to trusted contact number\n3.Must be add trusted contact number for emergency sms.\n4.Search police station area wise\n5. Search nearby all place with number and direction.\n6.Press power button up button for stop hidden camera\n7.Goto application manager and allow all permision");
                mybuilder.setIcon(R.drawable.log);
                mybuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                    }
                });

                mybuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       // Toast.makeText(getApplicationContext(),"I'm not goinf facking site",Toast.LENGTH_LONG).show();
                    }
                });
                AlertDialog mydialog= mybuilder.create();
                mydialog.show();


            }
        });


    }

    private void logoutActivity() {
       logout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               firebaseAuth.signOut();
               startActivity(new Intent(getApplicationContext(),LoginActivity.class));
           }
       });
    }

    private void nextActivity() {
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),HomePage.class));
            }
        });
    }

    @Override
    public void hearShake() {
        i++;
        if(i==3){
            try{
                scheduleAlarm();
                Toast.makeText(c, "Hidden camera is start", Toast.LENGTH_SHORT).show();
                Thread.sleep(500);
                callTo999();
                Toast.makeText(c, "Calling", Toast.LENGTH_SHORT).show();
                Thread.sleep(500);

                sendSms();
                Toast.makeText(c, "Message send to trusted number", Toast.LENGTH_SHORT).show();


            }catch (Exception e){
                e.printStackTrace();
            }
        }



    }
    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            Log.d("Test", "Long press!");
            Toast.makeText(getApplicationContext(), "Long KEYCODE_VOLUME_DOWN", Toast.LENGTH_LONG).show();
            flag = false;
            flag2 = true;
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_CAMERA){
            Toast.makeText(getApplicationContext(), "Long KEYCODE_CAMERA", Toast.LENGTH_LONG).show();
        }
        return super.onKeyLongPress(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            Toast.makeText(getApplicationContext(), "KEYCODE_VOLUME_DOWN", Toast.LENGTH_LONG).show();
            event.startTracking();
            if (flag2 == true) {
                flag = false;
            } else {

                flag = true;
                flag2 = false;
            }
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_CAMERA){
            Toast.makeText(getApplicationContext(), "KEYCODE_CAMERA", Toast.LENGTH_LONG).show();
        }
        if(keyCode == KeyEvent.KEYCODE_HEADSETHOOK)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_HEADSETHOOK", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_ENDCALL)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_ENDCALL", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BACK", Toast.LENGTH_SHORT).show();

        }
        if(keyCode == KeyEvent.KEYCODE_AVR_INPUT)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_AVR_INPUT", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_AVR_POWER)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_AVR_POWER", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BREAK)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BREAK", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BRIGHTNESS_DOWN)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BRIGHTNESS_DOWN", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BRIGHTNESS_UP)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BRIGHTNESS_UP", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_THUMBL)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_THUMBL", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_THUMBR)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_THUMBR", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_START)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_START", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_ENDCALL)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_ENDCALL", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_L1)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_L1", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_L2)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_L2", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_R1)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_R1", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_R2)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_R2", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_CALL)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_CALL", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_HOME", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_POWER)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_POWER", Toast.LENGTH_SHORT).show();
            //alertMethodForCall();
        }
        if(keyCode == KeyEvent.KEYCODE_SOFT_RIGHT)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SOFT_RIGHT", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_SOFT_LEFT)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SOFT_LEFT", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MENU)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MENU", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_PREVIOUS)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_PREVIOUS", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_NEXT)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_NEXT", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_PLAY)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_PLAY", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_PAUSE", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_PLAY_PAUSE", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_STOP)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_STOP", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_POUND)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_POUND", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_SETTINGS)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SETTINGS", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_SEARCH)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SEARCH", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_SLEEP)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SLEEP", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_STAR)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_STAR", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.	KEYCODE_TAB)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_TAB", Toast.LENGTH_SHORT).show();
        }

        if(keyCode==KeyEvent.KEYCODE_VOLUME_UP){
            //alertMethodForCall();
            cancelAlarm();
            Toast.makeText(this, "hidden camera stop", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_VOLUME_MUTE)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_VOLUME_MUTE", Toast.LENGTH_SHORT).show();
        }
        return super.onKeyDown(keyCode, event);
    }
    public void cancelAlarm() {
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            Toast.makeText(getApplicationContext(), "KEYCODE_VOLUME_UP", Toast.LENGTH_LONG).show();
            event.startTracking();
            if (flag) {
                Log.d("Test", "Short");
            }
            flag = true;
            flag2 = false;
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }


    private void scheduleAlarm() {

        ImageCaptureService.counter =0;
        calculateInterval();

        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis(); // alarm is set right away
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        int timeInterval =(int)(selectedIntervalMultiplier*60*1000);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, firstMillis,timeInterval , pIntent);
    }
    private void calculateInterval(){
        int index= ConfigPreferences.getInstance(this).getIntervalIndex();
        selectedIntervalMultiplier= ConfigActivity.intervalFactorList[index];
    }

    private void sendSms() {

        String msg = "I AM IN DANGER!";
        String contacts = dbHandler.getData();

        //Intent smsIntent=new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"+contacts));
        //smsIntent.putExtra("sms_body",msg);
        //startActivity(smsIntent);

        SmsManager sms = SmsManager.getDefault();

        sms.sendTextMessage(contacts,null,msg,null,null);
        Toast.makeText(this, "Message send successfully", Toast.LENGTH_SHORT).show();

    }

    private void callTo999() {

        String number = "999";
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + number));
        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }


}




