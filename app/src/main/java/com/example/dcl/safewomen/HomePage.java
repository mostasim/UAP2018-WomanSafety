package com.example.dcl.safewomen;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.dcl.safewomen.capture.ImageCaptureService;
import com.example.dcl.safewomen.configurations.ConfigActivity;
import com.example.dcl.safewomen.configurations.ConfigPreferences;
import com.example.dcl.safewomen.model.MyDBHandler;
import com.example.dcl.safewomen.scheduler.AlarmReceiver;
import com.squareup.seismic.ShakeDetector;

public class HomePage extends AppCompatActivity implements ShakeDetector.Listener{

   private ImageButton locationShare,callButton,policeStation,smsSend,capture,emergencyNum;
    int i = 0;
    Vibrator v;
    private MediaPlayer mediaPlayer;
    public static float selectedIntervalMultiplier = 0.0f;
    public final static int REQUEST_CODE = 10101;
    boolean flag = false;

    boolean flag2 = false;
    MyDBHandler dbHandler;
    CameraRecorder obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        dbHandler = new MyDBHandler(this,null,null,1);


        //obj= new CameraRecorder();

        locationShare=(ImageButton)findViewById(R.id.location);
        callButton=(ImageButton)findViewById(R.id.call);
        policeStation=(ImageButton)findViewById(R.id.police);
        smsSend=(ImageButton)findViewById(R.id.sms);
        capture=(ImageButton)findViewById(R.id.capture);
        emergencyNum=(ImageButton)findViewById(R.id.note);
        mediaPlayer= MediaPlayer.create(this,R.raw.alarm);
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        v = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        ShakeDetector sd = new ShakeDetector(this);
        sd.start(sensorManager);
        //initViewsClicks();

        emergencyNumberList();
        locationShare();
        calltoGovt();
        smsSendAction();
        //backgroundVideoRecoreder();
        captureImage();
        policeStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),SearchNearby.class));
            }
        });


    }


    @Override
    public void hearShake() {
        i++;
        //  v.vibrate(pattern,500);
        v.vibrate(500);
        //text.setText(""+i);

        if(i==3){


                try{


                    Thread.sleep(5000);
                    Toast.makeText(this, "Call to 999", Toast.LENGTH_SHORT).show();
                    callTo999();
                   Thread.sleep(5000);
                    Toast.makeText(this, "Camera started", Toast.LENGTH_SHORT).show();
                    scheduleAlarm();
                    Thread.sleep(5000);
                    //sendEmergencySms();
                    Toast.makeText(this, "Sms done", Toast.LENGTH_SHORT).show();
                    Thread.sleep(500);
                    //obj.startVideo();
                    //Toast.makeText(this, "Recording.....", Toast.LENGTH_SHORT).show();

                    //backgroundVideoRecoreder();
                    //Toast.makeText(this, "Video done", Toast.LENGTH_SHORT).show();



                }catch (Exception e){
                    e.printStackTrace();
                }



            //mediaPlayer.stop();



        }


    }

    private void sendEmergencySms(){


            String msg = "I AM IN DANGER!";
            String contacts = dbHandler.getData();

            //Intent smsIntent=new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"+contacts));
            //smsIntent.putExtra("sms_body",msg);
            //startActivity(smsIntent);

        SmsManager sms = SmsManager.getDefault();

        sms.sendTextMessage(contacts,null,msg,null,null);
        Toast.makeText(this, "Message send successfully", Toast.LENGTH_SHORT).show();


    }

    private void callTo999(){
        String number = "999";
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + number));
        if (ActivityCompat.checkSelfPermission(HomePage.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    private void emergencyNumberList() {

        emergencyNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(getApplicationContext(),analysisOfAI.class));
            }
        });

    }

    private void captureImage() {


        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ImageCapture.class));
            }
        });
    }

    private void backgroundVideoRecoreder() {

        //Intent intent = new Intent(HomePage.this, RecorderService.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //startService(intent);
        //finish();
    }

    private void smsSendAction() {


        smsSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    startActivity(new Intent(HomePage.this,SendSmsActivity.class));

            }
        });
    }
    //999 connect

    private void calltoGovt() {

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               callAction();
            }
        });
    }

    //location share


    private void locationShare() {

        locationShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ShareLocation.class));
            }
        });
    }


    private void callAction(){

        String number = "999";
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + number));
        if (ActivityCompat.checkSelfPermission(HomePage.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);




    }



    // This method binds start, stop and configuration buttons with their listeners.

    // This method finds interval multiplication factor based on user configuration settings.
    private void calculateInterval(){
        int index= ConfigPreferences.getInstance(this).getIntervalIndex();
        selectedIntervalMultiplier= ConfigActivity.intervalFactorList[index];
    }
    // This method configures AlarmManager which runs on periodic intervals and invokes Alarmreceiver broadcast class.
    private void scheduleAlarm() {
        ImageCaptureService.counter =0;
        calculateInterval();

        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis(); // alarm is set right away
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        int timeInterval =(int)(selectedIntervalMultiplier*60*1000);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, firstMillis,timeInterval , pIntent);




    }
    // This method is to cancel the Alarm when user clicks on stop service button
    public void cancelAlarm() {
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
    }

    private View.OnClickListener startServiceButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            scheduleAlarm();
        }
    };

    private View.OnClickListener stopServiceButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            cancelAlarm();
        }
    };
    private View.OnClickListener configClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            launchConfigActivity();
        }
    };

    // This launches the configuration screen that contains interval, size and flash settings.
    private void launchConfigActivity(){
        Intent intent = new Intent(HomePage.this,ConfigActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            Log.d("Test", "Long press!");
            Toast.makeText(getApplicationContext(), "Long KEYCODE_VOLUME_DOWN", Toast.LENGTH_LONG).show();
            flag = false;
            flag2 = true;
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_CAMERA){
            Toast.makeText(getApplicationContext(), "Long KEYCODE_CAMERA", Toast.LENGTH_LONG).show();
        }
        return super.onKeyLongPress(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            Toast.makeText(getApplicationContext(), "KEYCODE_VOLUME_DOWN", Toast.LENGTH_LONG).show();
            event.startTracking();
            if (flag2 == true) {
                flag = false;
            } else {

                flag = true;
                flag2 = false;
            }
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_CAMERA){
            Toast.makeText(getApplicationContext(), "KEYCODE_CAMERA", Toast.LENGTH_LONG).show();
        }
        if(keyCode == KeyEvent.KEYCODE_HEADSETHOOK)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_HEADSETHOOK", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_ENDCALL)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_ENDCALL", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            //stopService(new Intent(HomePage.this, RecorderService.class));
            //Toast.makeText(this, "Recording stoped", Toast.LENGTH_SHORT).show();
            //obj.stopVideo();


        }
        if(keyCode == KeyEvent.KEYCODE_AVR_INPUT)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_AVR_INPUT", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_AVR_POWER)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_AVR_POWER", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BREAK)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BREAK", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BRIGHTNESS_DOWN)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BRIGHTNESS_DOWN", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BRIGHTNESS_UP)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BRIGHTNESS_UP", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_THUMBL)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_THUMBL", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_THUMBR)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_THUMBR", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_START)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_START", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_ENDCALL)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_ENDCALL", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_L1)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_L1", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_L2)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_L2", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_R1)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_R1", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_BUTTON_R2)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_BUTTON_R2", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_CALL)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_CALL", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_HOME", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_POWER)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_POWER", Toast.LENGTH_SHORT).show();
            //alertMethodForCall();
        }
        if(keyCode == KeyEvent.KEYCODE_SOFT_RIGHT)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SOFT_RIGHT", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_SOFT_LEFT)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SOFT_LEFT", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MENU)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MENU", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_PREVIOUS)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_PREVIOUS", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_NEXT)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_NEXT", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_PLAY)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_PLAY", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_PAUSE", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_PLAY_PAUSE", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_MEDIA_STOP)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_MEDIA_STOP", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_POUND)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_POUND", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_SETTINGS)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SETTINGS", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_SEARCH)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SEARCH", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_SLEEP)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_SLEEP", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.KEYCODE_STAR)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_STAR", Toast.LENGTH_SHORT).show();
        }
        if(keyCode == KeyEvent.	KEYCODE_TAB)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_TAB", Toast.LENGTH_SHORT).show();
        }

        if(keyCode==KeyEvent.KEYCODE_VOLUME_UP){
            //alertMethodForCall();
            cancelAlarm();
            Toast.makeText(this, "hidden camera stop", Toast.LENGTH_SHORT).show();
        }
        

        if(keyCode == KeyEvent.KEYCODE_VOLUME_MUTE)
        {
            Toast.makeText(getApplicationContext(), "KEYCODE_VOLUME_MUTE", Toast.LENGTH_SHORT).show();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            Toast.makeText(getApplicationContext(), "KEYCODE_VOLUME_UP", Toast.LENGTH_LONG).show();
            event.startTracking();
            if (flag) {
                Log.d("Test", "Short");
            }
            flag = true;
            flag2 = false;
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }





}
