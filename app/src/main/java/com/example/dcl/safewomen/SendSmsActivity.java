package com.example.dcl.safewomen;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Toast;

import com.example.dcl.safewomen.model.MyDBHandler;

public class SendSmsActivity extends AppCompatActivity {

    MediaPlayer mediaPlayer;
    MyDBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);

        Toolbar toolbar= (Toolbar)findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        toolbar.setTitle(R.string.app_name);

        //mediaPlayer = MediaPlayer.create(this,R.raw.siren);
        dbHandler = new MyDBHandler(this,null,null,1);

    }

    public void playMusic(View v)
    {
        if(mediaPlayer.isPlaying())
            mediaPlayer.pause();
        else
            mediaPlayer.start();
    }

    public void sendHelp(View v)
    {
        String msg = "I AM IN DANGER!";
        String contacts = dbHandler.getData();

        //Intent smsIntent=new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"+contacts));
        //smsIntent.putExtra("sms_body",msg);
        //startActivity(smsIntent);

        SmsManager sms = SmsManager.getDefault();

        sms.sendTextMessage(contacts,null,msg,null,null);
        Toast.makeText(this, "Message send successfully", Toast.LENGTH_SHORT).show();

    }

    public void safeClicked(View v)
    {

        String msg = "I AM IN DANGER!";
        String contacts = dbHandler.getData();

        //Intent smsIntent=new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"+contacts));
        //smsIntent.putExtra("sms_body",msg);
        //startActivity(smsIntent);

        SmsManager sms = SmsManager.getDefault();

        sms.sendTextMessage(contacts,null,msg,null,null);
        Toast.makeText(this, "Message send successfully", Toast.LENGTH_SHORT).show();
    }

    public void nextActivity(View v)
    {
        Intent i =new Intent(this,Activity2.class);
        startActivity(i);
    }

}
