package com.example.dcl.safewomen;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.dcl.safewomen.capture.ImageCaptureService;
import com.example.dcl.safewomen.configurations.ConfigActivity;
import com.example.dcl.safewomen.configurations.ConfigPreferences;
import com.example.dcl.safewomen.scheduler.AlarmReceiver;

import java.io.File;

import static com.example.dcl.safewomen.R.layout.activity_image_capture;

public class ImageCapture extends AppCompatActivity {


    public static float selectedIntervalMultiplier = 0.0f;
    private Button video;

    // This is the first method in the activity that will be invoked.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_capture);


        initViewsClicks();
    }

    // This method binds start, stop and configuration buttons with their listeners.
    private void initViewsClicks() {
        final Button startServiceButton = (Button) findViewById(R.id.startButton);
        Button stopServiceButton = (Button) findViewById(R.id.stopButton);
        Button configButton = (Button) findViewById(R.id.configButton);
        video=(Button)findViewById(R.id.hiden);



        startServiceButton.setOnClickListener(startServiceButtonClickListener);
        stopServiceButton.setOnClickListener(stopServiceButtonClickListener);
        configButton.setOnClickListener(configClickListener);
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),CameraRecorder.class));
            }
        });
    }


    // This method finds interval multiplication factor based on user configuration settings.
    public void calculateInterval(){
        int index= ConfigPreferences.getInstance(this).getIntervalIndex();
        selectedIntervalMultiplier= ConfigActivity.intervalFactorList[index];
    }
    // This method configures AlarmManager which runs on periodic intervals and invokes Alarmreceiver broadcast class.
    public void scheduleAlarm() {
        ImageCaptureService.counter =0;
        calculateInterval();

        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis(); // alarm is set right away
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        int timeInterval =(int)(selectedIntervalMultiplier*60*1000);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, firstMillis,timeInterval , pIntent);
    }
    // This method is to cancel the Alarm when user clicks on stop service button
    public void cancelAlarm() {
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
    }

    private View.OnClickListener startServiceButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            scheduleAlarm();
        }
    };

    private View.OnClickListener stopServiceButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            cancelAlarm();
        }
    };
    private View.OnClickListener configClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            launchConfigActivity();
        }
    };

    // This launches the configuration screen that contains interval, size and flash settings.
    private void launchConfigActivity(){
        Intent intent = new Intent(ImageCapture.this,ConfigActivity.class);
        startActivity(intent);
    }
}
