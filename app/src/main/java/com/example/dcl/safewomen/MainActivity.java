package com.example.dcl.safewomen;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import static com.example.dcl.safewomen.ProfileActivity.REQUEST_CODE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText e1,e2;
    Button buttonRegister;
    TextView textViewSignup;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        //ActivityCompat.requestPermissions(MainActivity.this,new String[]{android.Manifest.permission.SEND_SMS},1);

        //if (checkDrawOverlayPermission()) {
          //  startService(new Intent(this, PowerButtonService.class));
        //}



        firebaseAuth= FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() !=null){
            finish();
            startActivity(new Intent(getApplicationContext(),ProfileActivity.class));

        }
        progressDialog= new ProgressDialog(this);
        e1=(EditText)findViewById(R.id.user_email);
        e2=(EditText)findViewById(R.id.user_pass);
        buttonRegister=(Button)findViewById(R.id.user_button);
        textViewSignup=(TextView)findViewById(R.id.txt_view);

        buttonRegister.setOnClickListener(this);
        textViewSignup.setOnClickListener(this);

    }





    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (!Settings.canDrawOverlays(this)) {
            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            /** request permission via start activity for result */
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }

    }







    @Override
    @TargetApi(Build.VERSION_CODES.M)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (Settings.canDrawOverlays(this)) {
                startService(new Intent(this, PowerButtonService.class));
            }
        }
    }


    private void registerUser(){


        String email= e1.getText().toString().trim();
        String pass= e2.getText().toString().trim();
        if(TextUtils.isEmpty(email)){
            Toast.makeText(getApplicationContext(),"Enter the email",Toast.LENGTH_LONG).show();
            return;

        }
        if(TextUtils.isEmpty(pass)){
            Toast.makeText(getApplicationContext(),"Enter the Password",Toast.LENGTH_LONG).show();
            return;

        }

        progressDialog.setMessage("Registering please wait.....");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){
                    //register successfuly completed
                    finish();
                    startActivity(new Intent(getApplicationContext(),ProfileActivity.class));



                }
                else{
                    Toast.makeText(MainActivity.this,"Couldn't sign in",Toast.LENGTH_LONG).show();

                }
                progressDialog.dismiss();


            }
        });



    }


    //




    @Override
    public void onClick(View view) {
        if(view==buttonRegister){
            //register method

            registerUser();
        }
        else if(view==textViewSignup){
            //Will open activity here
            startActivity(new Intent(MainActivity.this,LoginActivity.class));

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.logout) {
            //startActivity(new Intent(MainActivity.this, PostActivity.class));



        }
        return super.onOptionsItemSelected(item);
    }
}
